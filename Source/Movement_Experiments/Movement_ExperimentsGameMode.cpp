// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Movement_ExperimentsGameMode.h"
#include "Movement_ExperimentsHUD.h"
#include "Movement_ExperimentsCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMovement_ExperimentsGameMode::AMovement_ExperimentsGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMovement_ExperimentsHUD::StaticClass();
}
