// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Movement_ExperimentsGameMode.generated.h"

UCLASS(minimalapi)
class AMovement_ExperimentsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMovement_ExperimentsGameMode();
};



