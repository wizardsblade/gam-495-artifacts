// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Movement_ExperimentsHUD.generated.h"

UCLASS()
class AMovement_ExperimentsHUD : public AHUD
{
	GENERATED_BODY()

public:
	AMovement_ExperimentsHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

