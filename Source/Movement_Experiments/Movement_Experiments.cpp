// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Movement_Experiments.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Movement_Experiments, "Movement_Experiments" );
 