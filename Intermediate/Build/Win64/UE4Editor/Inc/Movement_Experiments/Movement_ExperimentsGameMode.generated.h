// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MOVEMENT_EXPERIMENTS_Movement_ExperimentsGameMode_generated_h
#error "Movement_ExperimentsGameMode.generated.h already included, missing '#pragma once' in Movement_ExperimentsGameMode.h"
#endif
#define MOVEMENT_EXPERIMENTS_Movement_ExperimentsGameMode_generated_h

#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_RPC_WRAPPERS
#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMovement_ExperimentsGameMode(); \
	friend struct Z_Construct_UClass_AMovement_ExperimentsGameMode_Statics; \
public: \
	DECLARE_CLASS(AMovement_ExperimentsGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Movement_Experiments"), MOVEMENT_EXPERIMENTS_API) \
	DECLARE_SERIALIZER(AMovement_ExperimentsGameMode)


#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMovement_ExperimentsGameMode(); \
	friend struct Z_Construct_UClass_AMovement_ExperimentsGameMode_Statics; \
public: \
	DECLARE_CLASS(AMovement_ExperimentsGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Movement_Experiments"), MOVEMENT_EXPERIMENTS_API) \
	DECLARE_SERIALIZER(AMovement_ExperimentsGameMode)


#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MOVEMENT_EXPERIMENTS_API AMovement_ExperimentsGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMovement_ExperimentsGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MOVEMENT_EXPERIMENTS_API, AMovement_ExperimentsGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMovement_ExperimentsGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MOVEMENT_EXPERIMENTS_API AMovement_ExperimentsGameMode(AMovement_ExperimentsGameMode&&); \
	MOVEMENT_EXPERIMENTS_API AMovement_ExperimentsGameMode(const AMovement_ExperimentsGameMode&); \
public:


#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MOVEMENT_EXPERIMENTS_API AMovement_ExperimentsGameMode(AMovement_ExperimentsGameMode&&); \
	MOVEMENT_EXPERIMENTS_API AMovement_ExperimentsGameMode(const AMovement_ExperimentsGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MOVEMENT_EXPERIMENTS_API, AMovement_ExperimentsGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMovement_ExperimentsGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMovement_ExperimentsGameMode)


#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_9_PROLOG
#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_RPC_WRAPPERS \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_INCLASS \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_INCLASS_NO_PURE_DECLS \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
