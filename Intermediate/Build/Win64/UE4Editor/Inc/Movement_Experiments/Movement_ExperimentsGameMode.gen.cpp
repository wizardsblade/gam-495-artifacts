// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Movement_Experiments/Movement_ExperimentsGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovement_ExperimentsGameMode() {}
// Cross Module References
	MOVEMENT_EXPERIMENTS_API UClass* Z_Construct_UClass_AMovement_ExperimentsGameMode_NoRegister();
	MOVEMENT_EXPERIMENTS_API UClass* Z_Construct_UClass_AMovement_ExperimentsGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Movement_Experiments();
// End Cross Module References
	void AMovement_ExperimentsGameMode::StaticRegisterNativesAMovement_ExperimentsGameMode()
	{
	}
	UClass* Z_Construct_UClass_AMovement_ExperimentsGameMode_NoRegister()
	{
		return AMovement_ExperimentsGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AMovement_ExperimentsGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMovement_ExperimentsGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Movement_Experiments,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMovement_ExperimentsGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Movement_ExperimentsGameMode.h" },
		{ "ModuleRelativePath", "Movement_ExperimentsGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMovement_ExperimentsGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMovement_ExperimentsGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMovement_ExperimentsGameMode_Statics::ClassParams = {
		&AMovement_ExperimentsGameMode::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008802A8u,
		nullptr, 0,
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AMovement_ExperimentsGameMode_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AMovement_ExperimentsGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMovement_ExperimentsGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMovement_ExperimentsGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMovement_ExperimentsGameMode, 1746425525);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMovement_ExperimentsGameMode(Z_Construct_UClass_AMovement_ExperimentsGameMode, &AMovement_ExperimentsGameMode::StaticClass, TEXT("/Script/Movement_Experiments"), TEXT("AMovement_ExperimentsGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMovement_ExperimentsGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
