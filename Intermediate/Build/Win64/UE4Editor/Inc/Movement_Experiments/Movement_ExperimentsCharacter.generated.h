// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MOVEMENT_EXPERIMENTS_Movement_ExperimentsCharacter_generated_h
#error "Movement_ExperimentsCharacter.generated.h already included, missing '#pragma once' in Movement_ExperimentsCharacter.h"
#endif
#define MOVEMENT_EXPERIMENTS_Movement_ExperimentsCharacter_generated_h

#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUnCrawl) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UnCrawl(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCrawl) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Crawl(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDoubleJump) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DoubleJump(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execWalk) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Walk(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSprint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Sprint(); \
		P_NATIVE_END; \
	}


#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUnCrawl) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UnCrawl(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCrawl) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Crawl(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDoubleJump) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DoubleJump(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execWalk) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Walk(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSprint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Sprint(); \
		P_NATIVE_END; \
	}


#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMovement_ExperimentsCharacter(); \
	friend struct Z_Construct_UClass_AMovement_ExperimentsCharacter_Statics; \
public: \
	DECLARE_CLASS(AMovement_ExperimentsCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Movement_Experiments"), NO_API) \
	DECLARE_SERIALIZER(AMovement_ExperimentsCharacter)


#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAMovement_ExperimentsCharacter(); \
	friend struct Z_Construct_UClass_AMovement_ExperimentsCharacter_Statics; \
public: \
	DECLARE_CLASS(AMovement_ExperimentsCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Movement_Experiments"), NO_API) \
	DECLARE_SERIALIZER(AMovement_ExperimentsCharacter)


#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMovement_ExperimentsCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMovement_ExperimentsCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMovement_ExperimentsCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMovement_ExperimentsCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMovement_ExperimentsCharacter(AMovement_ExperimentsCharacter&&); \
	NO_API AMovement_ExperimentsCharacter(const AMovement_ExperimentsCharacter&); \
public:


#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMovement_ExperimentsCharacter(AMovement_ExperimentsCharacter&&); \
	NO_API AMovement_ExperimentsCharacter(const AMovement_ExperimentsCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMovement_ExperimentsCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMovement_ExperimentsCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMovement_ExperimentsCharacter)


#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(AMovement_ExperimentsCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(AMovement_ExperimentsCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(AMovement_ExperimentsCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(AMovement_ExperimentsCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(AMovement_ExperimentsCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AMovement_ExperimentsCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(AMovement_ExperimentsCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(AMovement_ExperimentsCharacter, L_MotionController); }


#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_11_PROLOG
#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_RPC_WRAPPERS \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_INCLASS \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_INCLASS_NO_PURE_DECLS \
	Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Movement_Experiments_Source_Movement_Experiments_Movement_ExperimentsCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
