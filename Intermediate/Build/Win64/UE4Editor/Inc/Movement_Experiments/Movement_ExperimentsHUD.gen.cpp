// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Movement_Experiments/Movement_ExperimentsHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovement_ExperimentsHUD() {}
// Cross Module References
	MOVEMENT_EXPERIMENTS_API UClass* Z_Construct_UClass_AMovement_ExperimentsHUD_NoRegister();
	MOVEMENT_EXPERIMENTS_API UClass* Z_Construct_UClass_AMovement_ExperimentsHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_Movement_Experiments();
// End Cross Module References
	void AMovement_ExperimentsHUD::StaticRegisterNativesAMovement_ExperimentsHUD()
	{
	}
	UClass* Z_Construct_UClass_AMovement_ExperimentsHUD_NoRegister()
	{
		return AMovement_ExperimentsHUD::StaticClass();
	}
	struct Z_Construct_UClass_AMovement_ExperimentsHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMovement_ExperimentsHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_Movement_Experiments,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMovement_ExperimentsHUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "Movement_ExperimentsHUD.h" },
		{ "ModuleRelativePath", "Movement_ExperimentsHUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMovement_ExperimentsHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMovement_ExperimentsHUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMovement_ExperimentsHUD_Statics::ClassParams = {
		&AMovement_ExperimentsHUD::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008002ACu,
		nullptr, 0,
		nullptr, 0,
		"Game",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AMovement_ExperimentsHUD_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AMovement_ExperimentsHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMovement_ExperimentsHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMovement_ExperimentsHUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMovement_ExperimentsHUD, 2036159906);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMovement_ExperimentsHUD(Z_Construct_UClass_AMovement_ExperimentsHUD, &AMovement_ExperimentsHUD::StaticClass, TEXT("/Script/Movement_Experiments"), TEXT("AMovement_ExperimentsHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMovement_ExperimentsHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
